import 'package:flutter/material.dart';

class User{

  String name;
  Map<String, dynamic>  photo;

  User({ @required this.name, @required this.photo});

  factory User.fromJson(Map<String, dynamic> jsonData){
    return User( name: jsonData['username'], photo: jsonData['picture']);
  }
}