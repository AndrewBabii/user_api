import 'package:flutter/material.dart';
import 'package:norm_api/user.dart';
import 'package:norm_api/user_repository.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

List<User> items = [];

Future<void> _getUsers() async {
  List<User> users = await UserRepository().getUsers();
  items = users;
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    _getUsers().then((_) {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        width: 200,
        height: 200,
        child: items.length < 1
            ? CircularProgressIndicator()
            : ListView.builder(itemBuilder: (ctx, index) {
                return Column(children: [
                  Text(items[index].name),
                  Image.network(
                      items[index].photo['medium']),


                ]);
              },itemCount: items.length),

      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
