import 'dart:convert';

import 'user.dart';
import 'htttp_service.dart';

class UserRepository{
  Future<List<User>> getUsers() async{
    List<User> users =[];

    var response = await HttpService().getUsers();
    List jsonData = json.decode(response);

    for(int i =0; i< jsonData.length; i++){
      users.add(User.fromJson(jsonData[i]));
    }

  }
}