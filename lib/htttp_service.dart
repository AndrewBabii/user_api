import 'user.dart';
import 'package:http/http.dart' as http;

class HttpService{
  final String usersUrl = 'https://randomuser.me/api/?gender=female';

  Future<dynamic> getUsers() async{
    var response = await http.get('https://randomuser.me/api/?gender=female');
    return response.body;
  }
}